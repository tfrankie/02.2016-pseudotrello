app.controller('navbarCtrl', ['$scope', '$location', 'userModel', '$mdSidenav', function($scope, $location, userModel, $mdSidenav){

	if($scope.isEmpty(userModel.fullName)){
		$scope.fullName = "Profile";
	} else {
		$scope.fullName = userModel.fullName;
	}

	$scope.openSidebar = function() {
		$mdSidenav('sidebar').toggle();
	};
	
	$scope.logout = function() {
		userModel.removeAuth();
		$location.path('/');
    };

	$scope.$watch(function(){return userModel.fullName}, function(){
		if(!$scope.isEmpty(userModel.fullName)){
			$scope.fullName = userModel.fullName;
		}
	});

}]);
