app.controller('settingsCtrl', ['$scope', 'focus', 'userModel', 'ngDialog', function($scope, focus, userModel, ngDialog){

    $scope.newNameSettings = userModel.fullName;

    $scope.changeName = function(){
        if($scope.isEmpty($scope.newNameSettings)){
            focus('edit-name-settings');
        } else {
            var userData = {
                login: userModel.getLogin(),
                fullName: $scope.newNameSettings
            };
            userModel.changeUserName(userData).then(
                function(response) {
                    userModel.setFullName(response.data.fullName);
                    $scope.newNameSettings = userModel.fullName;

                    openDialog('alert alert-success', 'Name <strong>successfully</strong> changed.');
                }
            );
        }
    };
    $scope.validOldPassword = false;
    $scope.validNewPassword = false;
    $scope.validConfirmedPassword = false;

    $scope.changePassword = function(){
        if($scope.validOldPassword && $scope.validNewPassword && $scope.validConfirmedPassword){
            var changePasswordData = {
                login: userModel.getLogin(),
                oldPassword:$scope.oldPasswordSettings,
                newPassword: $scope.newPasswordSettings
            };
            userModel.changeUserPassword(changePasswordData).then(function(){
                openDialog('alert alert-success', 'Password <strong>successfully</strong> changed.');
                $scope.oldPasswordSettings = "";
                $scope.newPasswordSettings = "";
                $scope.confirmPasswordSettings = "";
            }, function(response){
                var message = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> ' + response.data.message;
                openDialog('alert alert-warning', message);
            });
        } else if(!$scope.validOldPassword){
            focus('old-password-settings');
            openDialog('alert alert-warning', 'Password has minimum <strong>5</strong> signs.');
        } else if(!$scope.validNewPassword){
            focus('new-password-settings');
            openDialog('alert alert-warning', 'New password should have minimum <strong>5</strong> signs.');
        } else if(!$scope.validConfirmedPassword){
            focus('confirm-password-settings');
            openDialog('alert alert-warning', 'Passwords should <strong>match</strong>.');
        }
    };

    $scope.$watch('oldPasswordSettings', function(newVal, oldVal){
        if(!$scope.isEmpty(newVal)){
            if( newVal.length >= 5){
                $scope.validOldPassword = true;
            } else {
                $scope.validOldPassword = false;
            }
        } else {
            $scope.validOldPassword = false;
        }
    });

    $scope.$watch('newPasswordSettings', function(newVal, oldVal){
        if(!$scope.isEmpty(newVal)){
            if( newVal.length >= 5){
                $scope.validNewPassword = true;
            } else {
                $scope.validNewPassword = false;
            }
            if( newVal === $scope.confirmPasswordSettings){
                $scope.validConfirmedPassword = true;
            } else {
                $scope.validConfirmedPassword = false;
            }
        } else {
            $scope.validNewPassword = false;
        }
    });

    $scope.$watch('confirmPasswordSettings', function(newVal, oldVal){
        if(!$scope.isEmpty(newVal)){
            if( newVal === $scope.newPasswordSettings){
                $scope.validConfirmedPassword = true;
            } else {
                $scope.validConfirmedPassword = false;
            }
        } else {
            $scope.validConfirmedPassword = false;
        }
    });


    function openDialog(className, message){
        var template = '<div class="' + className + '">' + message +'</div>';
        ngDialog.open({
            template: template,
            plain: true
        });
    }
}]);