app.controller('dashboardCtrl', ['$scope', '$cookieStore', '$location', 'userModel', 'focus', function($scope, $cookieStore, $location, userModel, focus){

    var user = {
        login: $cookieStore.get('auth').login
    };
    userModel.getDashboards(user).then(
        function(response) {
            $scope.dashboards = response.data;
        }
    );

    $scope.onDashboardClick = function (dashboardId) {
        $location.path('/dashboard/' + dashboardId);
    };

    $scope.addNewDashboard = function(){
        if($scope.isEmpty($scope.newDashboardName)){
            focus('new-name-input');
        } else {
            var dashboardData = {
                user: user.login,
                dashboardName: $scope.newDashboardName
            };
            userModel.addNewDashboard(dashboardData).then(
                function(response) {
                    $scope.dashboards.push(response.data);
                }
            );
            $scope.hideNewDashboardForm();
        }
    };

    $scope.showNewDashboardForm = function () {
        $scope.addNewDashboardVisibility = true;
    };
    $scope.hideNewDashboardForm = function(){
        $scope.newDashboardName = "";
        $scope.addNewDashboardVisibility = false;
    };
}]);


app.controller('dashboardButtonCtrl', ['$scope', 'userModel', function($scope, userModel){

    $scope.showButtons = function() {
        if(!($scope.editButtonsVisibility || $scope.deleteButtonsVisibility)){
            $scope.mainButtonsVisibility = true;
        }
    };
    $scope.hideButtons = function(){
        $scope.mainButtonsVisibility = false;
    };

    $scope.editDashboard = function(event){
        event.preventDefault();
        event.stopPropagation();
        $scope.editButtonsVisibility = true;
        $scope.mainButtonsVisibility = false;

        $scope.newDashboardName = $scope.dashboard.name;
    };
    $scope.saveDashboard = function(event, dashboardId){
        event.preventDefault();
        event.stopPropagation();

        if($scope.editButtonsVisibility){
            var editDashboardData = {
                dashboardId: dashboardId,
                name: $scope.newDashboardName
            };
            userModel.editDashboardName(editDashboardData).then(
                function(response){
                    for(var i = 0; i < $scope.dashboards.length; i++) {
                        var dashboard = $scope.dashboards[i];
                        if(dashboard.dashboardId == response.data.dashboardId){
                            $scope.dashboards[i].name = response.data.name;
                        }
                    }
                    $scope.editButtonsVisibility = false;
                    $scope.mainButtonsVisibility = true;
                }
            );
        } else {
            var dashboardData = {
                dashboardId: dashboardId
            };
            userModel.deleteDashboard(dashboardData).then(
                function(response){
                    for(var i = 0; i < $scope.dashboards.length; i++) {
                        var dashboard = $scope.dashboards[i];
                        if(dashboard.dashboardId == response.data.dashboardId){
                            $scope.dashboards.splice(i,1);
                        }
                    }
                }
            );
        }

    };
    $scope.cancelEditingDashboard = function(event){
        event.preventDefault();
        event.stopPropagation();
        $scope.editButtonsVisibility = false;
        $scope.deleteButtonsVisibility = false;
        $scope.mainButtonsVisibility = true;
    };
    $scope.cancelPagination = function(event){
        event.preventDefault();
        event.stopPropagation();
    };


    $scope.deleteDashboard = function(event){
        event.preventDefault();
        event.stopPropagation();

        $scope.deleteButtonsVisibility = true;
        $scope.mainButtonsVisibility = false;
    };

}]);