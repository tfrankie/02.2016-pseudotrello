app.controller('noteCtrl', ['$scope', 'ngDialog', 'userModel', 'focus', function($scope, ngDialog, userModel, focus){

    $scope.note = $scope.ngDialogData.note;
    $scope.editButtonText = "Edit";
    $scope.deleteButtonText = "Delete";

    $scope.editNoteClicked = function(){
        if($scope.editButtonsVisibility){
            if(!$scope.isEmpty($scope.editNoteTitle)){
                $scope.editButtonsVisibility = false;
                $scope.editButtonText = "Edit";
                $scope.deleteButtonText = "Delete";

                var editNoteData = {
                    noteId: $scope.note.id,
                    title: $scope.editNoteTitle,
                    description: $scope.editNoteDescription
                };
                userModel.editNote(editNoteData).then(
                    function(response) {
                        $scope.note.title = response.data.title;
                        $scope.note.description = response.data.description;
                    }
                );
            } else {
                focus('edit-note-title');
            }
        } else {
            $scope.editButtonsVisibility = true;
            $scope.editButtonText = "Save";
            $scope.deleteButtonText = "Cancel";

            $scope.editNoteTitle = $scope.note.title;
            $scope.editNoteDescription = $scope.note.description;
        }
    };

    $scope.deleteNoteClicked = function(){
        if($scope.editButtonsVisibility){
            $scope.editButtonsVisibility = false;
            $scope.editButtonText = "Edit";
            $scope.deleteButtonText = "Delete";
        } else {
            ngDialog.open({
                template:
                '<p>Are you sure you want to delete this note?</p>' +
                '<div class="ngdialog-buttons">' +
                '<button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">No</button>' +
                '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeAllDialogs()">Yes</button>' +
                '</div>',
                scope: $scope,
                controller: 'nestedDialogCtrl',
                plain: true
            });
        }
    };

    $scope.deleteNoteConfirmed = function(){
        var deleteNoteData = {
            noteId: $scope.note.id
        };
        userModel.deleteNote(deleteNoteData).then(
            function(response) {
                for(var i = 0; i < $scope.lists.length; i++) {
                    var noteList = $scope.lists[i].noteList;
                    for(var j = 0; j < noteList.length; j++) {
                        var note = noteList[j];
                        if(note.id == response.data.noteId){
                            noteList.splice(j,1);
                        }
                    }
                }
            }
        );
    }

}]);

app.controller('nestedDialogCtrl', function ($scope, ngDialog) {

    $scope.closeAllDialogs = function(){
        $scope.deleteNoteConfirmed();
        ngDialog.close();
    };

});