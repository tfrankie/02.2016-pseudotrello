app.controller('sidebarCtrl', function ($scope, $mdSidenav, userModel, ngDialog) {

    if($scope.isEmpty(userModel.fullName)){
        $scope.fullName = "Profile";
    } else {
        $scope.fullName = userModel.fullName;
    }

    $scope.close = function () {
        $mdSidenav('sidebar').close();
    };

    $scope.onProfileClick = function () {
        ngDialog.open({
            template: 'profile',
            controller: 'profileCtrl',
            className: 'ngdialog-theme-default trello-dialog',
            scope: $scope
        });
        $mdSidenav('sidebar').close();
    };
    $scope.onSettingsClick = function () {
        ngDialog.open({
            template: 'settings',
            controller: 'settingsCtrl',
            width: 480,
            scope: $scope
        });
        $mdSidenav('sidebar').close();
    };
    $scope.onAboutClick = function () {
        ngDialog.open({
            template: 'about',
            className: 'ngdialog-theme-default trello-dialog'
        });
        $mdSidenav('sidebar').close();
    };

    $scope.$watch(function(){return userModel.fullName}, function(){
        if(!$scope.isEmpty(userModel.fullName)){
            $scope.fullName = userModel.fullName;
        }
    });

});