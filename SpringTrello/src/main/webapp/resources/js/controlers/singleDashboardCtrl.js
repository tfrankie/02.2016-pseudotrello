app.controller('singleDashboardCtrl', ['$scope', '$routeParams', 'userModel', 'ngDialog', 'focus', function($scope, $routeParams, userModel, ngDialog, focus){

    userModel.getLists($routeParams.dashboardId).then(
        function(response) {
            $scope.lists = response.data;
        }
    );

    $scope.addNewList = function(){
        if($scope.isEmpty($scope.newListName)){
            focus('new-list-input');
        } else {
            var listData = {
                dashboardId: $routeParams.dashboardId,
                listName: $scope.newListName
            };
            userModel.addNewList(listData).then(
                function(response) {
                    $scope.lists.push(response.data);
                }
            );
            $scope.hideNewListForm();
        }
    };

    $scope.showNewListForm = function(){
        $scope.addNewListVisibility = true;
    };
    $scope.hideNewListForm = function(){
        $scope.newListName = "";
        $scope.addNewListVisibility = false;
    };

    $scope.onNoteClick = function(note){
        ngDialog.open({
            template: 'note',
            controller: 'noteCtrl',
            className: 'ngdialog-theme-default trello-dialog',
            scope: $scope,
            data: {
                note: note
            }
        });
    };

    $scope.dragStart = function(listId){
        $scope.currentDraggingList = listId;
    };

}]);

app.controller('singleListController', ['$scope', 'userModel', 'focus', function($scope, userModel, focus) {

    $scope.showButtons = function() {
        if(!($scope.editButtonsVisibility || $scope.deleteButtonsVisibility)){
            $scope.mainButtonsVisibility = true;
        }
    };
    $scope.hideButtons = function(){
        $scope.mainButtonsVisibility = false;
    };

    $scope.editList = function(){
        $scope.editButtonsVisibility = true;
        $scope.mainButtonsVisibility = false;
        $scope.newListName = $scope.list.listName;
    };

    $scope.cancelEditingList = function(){
        $scope.editButtonsVisibility = false;
        $scope.deleteButtonsVisibility = false;
        $scope.mainButtonsVisibility = true;
    };

    $scope.saveList = function(listId){
        if($scope.editButtonsVisibility){
            var editListData = {
                listId: listId,
                listName: $scope.newListName
            };
            userModel.editListName(editListData).then(
                function(response){
                    for(var i = 0; i < $scope.lists.length; i++) {
                        var list = $scope.lists[i];
                        if(list.listId == response.data.listId){
                            $scope.lists[i].listName = response.data.listName;
                        }
                    }
                    $scope.editButtonsVisibility = false;
                    $scope.mainButtonsVisibility = true;
                }
            );
        } else {
            var listData = {
                listId: listId
            };
            userModel.deleteList(listData).then(
                function(response){
                    for(var i = 0; i < $scope.lists.length; i++) {
                        var list = $scope.lists[i];
                        if(list.listId == response.data.listId){
                            $scope.lists.splice(i,1);
                        }
                    }
                }
            );
        }
    };

    $scope.deleteList = function(){
        $scope.deleteButtonsVisibility = true;
        $scope.mainButtonsVisibility = false;
    };


    $scope.addNewNote = function(listId, count){
        if($scope.isEmpty($scope.newNoteText)){
            focus('new-note-input');
        } else {
            var newNoteData = {
                listId: listId,
                title: $scope.newNoteText,
                position: count
            };
            userModel.addNewNote(newNoteData).then(
                function(response){
                    for(var i = 0; i < $scope.lists.length; i++) {
                        var list = $scope.lists[i];
                        if(list.listId == response.data.listId){
                            var note = {
                                title: response.data.title,
                                id: response.data.noteId
                            };
                            list.noteList.push(note);
                            $scope.addNewNoteVisibility = false;
                            $scope.newNoteText = "";
                        }
                    }
                }
            );
        }
    };
    $scope.cancelAddingNewNote = function(){
        $scope.addNewNoteVisibility = false;
        $scope.newNoteText = "";
    };

    $scope.noteDroped = function(listId, item, index){
        if(listId == $scope.currentDraggingList){
            if(index >= item.position){
                index--;
            }
        }
        var changeNotePositionData = {
            noteId: item.id,
            listId: listId,
            position: index
        };
        userModel.changeNotePosition(changeNotePositionData);
        item.position = index;
        return item;
    };

}]);
