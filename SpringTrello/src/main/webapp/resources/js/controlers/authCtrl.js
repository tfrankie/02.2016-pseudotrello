app.controller('authCtrl', ['$scope', '$location', 'userModel', 'Flash', 'focus', function($scope, $location, userModel, Flash, focus){

		$scope.login = function() {
			if($scope.isEmpty($scope.enteredLogin)){
				focus('login-login')
			} else if($scope.isEmpty($scope.enteredPassword)){
				focus('login-pass')
			} else {
		    	var user = {
		    		login: $scope.enteredLogin,
		    		password: $scope.enteredPassword
		    	};
				userModel.postLogin(user).then(function(response){
					userModel.putAuth(response.data);
					$location.path('/dashboard');
				},
				function() {
					var message = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><strong> Oups!</strong> Wrong login or password.';
					Flash.create('danger', message);
				});
			}
	    };
	    
	    $scope.register = function() {
			if($scope.validLoginText && $scope.validPasswordText && $scope.validRepeatedPasswordText){
		    	var user = {
		    		login: $scope.enteredLogin,
		    		password: $scope.enteredPassword
		    	};
		    	userModel.postRegister(user).then(function(){
					Flash.create('success', 'Account <strong>successfully</strong> created.');
		 			$scope.showLoginForm();
				}, function(){
					var message = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><strong> Username</strong> already exists.';
					Flash.create('danger', message);
			        $scope.enteredLogin = "";
				});
			} else if(!$scope.validLoginText){
				focus('register-login')
			} else if(!$scope.validPasswordText){
				focus('register-pass')
			} else if(!$scope.validRepeatedPasswordText){
				focus('register-rep-pass')
			}
	    };
	    
	    $scope.showRegisterForm = function(){
	    	clearLoginPasswords();
	    	setRegisterFormVisibility(true);
	    };
	    $scope.showLoginForm = function(){
	    	clearLoginPasswords();
	    	setRegisterFormVisibility(false);
	    };
	    
	    $scope.validLoginText = false;
		$scope.validPasswordText = false;
		$scope.validRepeatedPasswordText = false;
		
		$scope.$watch('enteredLogin', function(newVal, oldVal){
			if(!$scope.isEmpty(newVal)){
				if( newVal.length >= 5){
					$scope.validLoginText = true;
				} else {
					$scope.validLoginText = false;
				}
			} else {
				$scope.validLoginText = false;
			}
		});
		$scope.$watch('enteredPassword', function(newVal, oldVal){
			if(!$scope.isEmpty(newVal)){
				if( newVal.length >= 5){
					$scope.validPasswordText = true;
				} else {
					$scope.validPasswordText = false;
				}
				if( newVal === $scope.enteredRepeatedPassword){
					$scope.validRepeatedPasswordText = true;
				} else {
					$scope.validRepeatedPasswordText = false;
				}
			} else {
				$scope.validPasswordText = false;
			}
		});
		$scope.$watch('enteredRepeatedPassword', function(newVal, oldVal){
			if(!$scope.isEmpty(newVal)){
				if( newVal === $scope.enteredPassword){
					$scope.validRepeatedPasswordText = true;
				} else {
					$scope.validRepeatedPasswordText = false;
				}
			} else {
				$scope.validRepeatedPasswordText = false;
			}
		});
		
	    function clearLoginPasswords(){
	    	$scope.enteredLogin = "";
	    	$scope.enteredPassword = "";
			$scope.enteredRepeatedPassword = "";
	    }
	    function setRegisterFormVisibility(visibility){
	    	$scope.registerFormVisibility = visibility;
	    }
	    
	}]);
	