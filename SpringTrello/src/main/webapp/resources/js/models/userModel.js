app.factory('userModel', ['$http', '$cookieStore',  function($http, $cookieStore){
	
	var userModel = {};

	if($cookieStore.get('auth')){
		userModel.fullName = $cookieStore.get('auth').fullName;
	}

	userModel.postLogin= function(user){
		return postMethod(user, '/login', 'POST');
    };

	userModel.postRegister = function(user){
		return postMethod(user, '/register', 'POST');
    };

	userModel.getDashboards = function(user){
		return postMethod(user, '/getDashboards', 'POST');
	};

	userModel.getLists = function(dashboardId){
		return postMethod(dashboardId, '/getLists', 'POST');
	};

	userModel.addNewDashboard = function(dashboardData){
		return postMethod(dashboardData, '/addNewDashboard', 'PUT');
	};

	userModel.addNewList = function(listData){
		return postMethod(listData, '/addNewList', 'PUT');
	};

	userModel.deleteDashboard = function(deleteDashboardData){
		return postMethod(deleteDashboardData, '/deleteDashboard', 'POST');
	};

	userModel.editDashboardName = function(changingDashboardData){
		return postMethod(changingDashboardData, '/editDashboard', 'PUT');
	};

	userModel.deleteList = function(deleteListData){
		return postMethod(deleteListData, '/deleteList', 'POST');
	};

	userModel.editListName = function(changingListData){
		return postMethod(changingListData, '/editList', 'PUT');
	};

	userModel.addNewNote = function(newNoteData){
		return postMethod(newNoteData, '/addNewNote', 'PUT');
	};

	userModel.changeNotePosition = function(changeNotePositionData){
		return postMethod(changeNotePositionData, '/changeNotePosition', 'PUT');
	};

	userModel.deleteNote = function(deleteNoteData){
		return postMethod(deleteNoteData, '/deleteNote', 'DELETE');
	};

	userModel.editNote = function(editNoteData){
		return postMethod(editNoteData, '/editNote', 'PUT');
	};

	userModel.changeUserName = function(userData){
		return postMethod(userData, '/changeUserName', 'PUT');
	};

	userModel.changeUserPassword = function(changePasswordData){
		return postMethod(changePasswordData, '/changeUserPassword', 'PUT');
	};

    userModel.getAuthStatus = function(){
    	var status = $cookieStore.get('auth');
    	if(status){
    		return true;
    	} else {
    		return false;
    	}
    };

	userModel.getLogin = function(){
		return $cookieStore.get('auth').login;
	};

	userModel.setFullName = function(value){
		var newCookie = $cookieStore.get('auth');
		newCookie.fullName = value;

		userModel.putAuth(newCookie);
	};

	userModel.removeAuth = function(){
		$cookieStore.remove('auth');
	};

	userModel.putAuth = function(value){
		$cookieStore.put('auth', value);

		userModel.fullName = value.fullName;
	};

	function postMethod(data, url, method){
		return $http({
			method: method,
			url: url,
			data: data,
			headers: {'Accept': 'application/json',
				'Content-Type': 'application/json' }
		});
	}

	return userModel;
}]);