var app = angular.module('myApp', ['ngRoute', 'ngCookies', 'ngAnimate', 'ngFlash', 'ngDialog' , 'dndLists', 'ngMaterial']);

app.run(function($rootScope) {
	$rootScope.isEmpty = function(str) {
		return (!str || 0 === str.length);
	};
});

app.config(['$routeProvider', function($routeProvider) {
		$routeProvider.when('/', {
    	   templateUrl: 'login',
    	   controller: 'authCtrl'
		});
		$routeProvider.when('/dashboard', {
    	   templateUrl: 'dashboard',
    	   controller: 'dashboardCtrl',
    	   authenticated: true
		});
		$routeProvider.when('/dashboard/:dashboardId', {
			templateUrl: function(params){ return 'dashboard/' + params.dashboardId; },
			controller: 'singleDashboardCtrl'
		});
		$routeProvider.when('/profile', {
    	   templateUrl: 'profile',
    	   controller: 'profileCtrl',
    	   authenticated: true
		});
		$routeProvider.otherwise({
			redirectTo: '/'
		});
}]);

app.run(['$rootScope', '$location', 'userModel', function($rootScope, $location, userModel){
	$rootScope.$on("$routeChangeStart", function(event, next, current){
		
		if(next.$$route.authenticated){
			if(!userModel.getAuthStatus()){
				$location.path('/');
			}
		}
		if(next.$$route.originalPath == '/'){
			if(userModel.getAuthStatus()){
				$location.path('/dashboard');
			}
		}
	
	})
}]);

//app.directive('myFocus', function () {
//    return {
//      restrict: 'A',
//      link: function postLink(scope, element, attrs) {
//        if (attrs.myFocus == "") {
//          attrs.myFocus = "focusElement";
//        }
//        scope.$watch(attrs.myFocus, function(value) {
//          if(value == attrs.id) {
//            element[0].focus();
//          }
//        });
//        element.on("blur", function() {
//          scope[attrs.myFocus] = "";
//          scope.$apply();
//        })
//      }
//    };
//});

app.factory('focus', function($timeout, $window) {
	return function(id) {
		// timeout makes sure that it is invoked after any other event has been triggered.
		// e.g. click events that need to run before the focus or
		// inputs elements that are in a disabled state but are enabled when those events
		// are triggered.
		$timeout(function() {
			var element = $window.document.getElementById(id);
			if(element)
				element.focus();
		});
	};
});

app.directive('eventFocus', function(focus) {
	return function(scope, elem, attr) {
		elem.on(attr.eventFocus, function() {
			focus(attr.eventFocusId);
		});

		// Removes bound events in the element itself
		// when the scope is destroyed
		scope.$on('$destroy', function() {
			elem.off(attr.eventFocus);
		});
	};
});