<!DOCTYPE html>
<html ng-app="myApp">
	<head>
		<script src="${pageContext.request.contextPath}/resources/js/libs/angular.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/libs/angular-route.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/libs/angular-cookies.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/libs/angular-animate.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/libs/angular-flash.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/libs/angular-drag-and-drop-lists.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/libs/ngDialog.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/libs/angular-material.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/libs/angular-aria.js"></script>

		<script src="${pageContext.request.contextPath}/resources/js/jquery-1.12.3.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

		<script src="${pageContext.request.contextPath}/resources/js/app.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/controlers/authCtrl.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/controlers/navbarCtrl.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/controlers/dashboardCtrl.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/controlers/singleDashboardCtrl.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/controlers/profileCtrl.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/controlers/settingsCtrl.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/controlers/noteCtrl.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/controlers/sidebarCtrl.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/models/userModel.js"></script>

		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/libs/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/libs/ngDialog.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/libs/ngDialog-theme-default.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/libs/angular-material.css">

		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/app.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/auth.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/dashboard.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bars.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/note.css">
        
		<title>MyTrello</title>
	</head>
	<body>

		<div ng-view></div>

	</body>
</html>