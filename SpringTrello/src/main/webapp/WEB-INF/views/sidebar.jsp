<md-sidenav class="md-sidenav-left md-whiteframe-z2" md-component-id="sidebar">

    <nav class="navbar navbar-default" role="navigation">
        <div class = "btn btn-default navbar-btn navbar-center" ng-click="close()" >
            Close
        </div>
    </nav>


    <md-content>
        <div class = "sidebar-name">
            {{ fullName }}
        </div>
        <div class = "btn btn-default sidebar-button" ng-click="onProfileClick()" >
            Profile
        </div>
        <div class = "btn btn-default sidebar-button" ng-click="onSettingsClick()" >
            Settings
        </div>
        <hr>
        <div class = "btn btn-default sidebar-button" ng-click="onAboutClick()" >
            About
        </div>
    </md-content>

</md-sidenav>