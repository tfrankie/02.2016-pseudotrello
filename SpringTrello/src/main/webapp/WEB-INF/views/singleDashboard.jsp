<div ng-include src="'sidebar'" ng-controller="sidebarCtrl"></div>
<div ng-include src="'navbar'" ng-controller="navbarCtrl"></div>

<div class="row dsh-row">
    <div ng-repeat="list in lists" class="col-md-3">

        <div class="list" ng-controller="singleListController">

            <%--LIST TITLE--%>
            <div class="list-title" ng-mouseover="showButtons()" ng-mouseleave = "hideButtons()" >
                <span ng-show="editButtonsVisibility || deleteButtonsVisibility" ng-click="cancelEditingList()" class="glyphicon glyphicon-remove glyph-red inner-button"></span>
                <span ng-show="mainButtonsVisibility" ng-click="editList()" class="glyphicon glyphicon-edit glyph-blue inner-button"></span>

                <input class="edit-input list-input" ng-show="editButtonsVisibility && !deleteButtonsVisibility" id="edit-list-input" type="text" ng-model="newListName" placeholder="list name"/>
                <span ng-hide="editButtonsVisibility || deleteButtonsVisibility">{{  list.listName  }} </span>
                <span ng-show="deleteButtonsVisibility" class="deleteDashboard" >DELETE THIS?</span>

                <span ng-show="editButtonsVisibility || deleteButtonsVisibility" ng-click="saveList(list.listId)" class="glyphicon glyphicon-ok glyph-green inner-button"></span>
                <span ng-show="mainButtonsVisibility" ng-click="deleteList()" class="glyphicon glyphicon-remove glyph-red inner-button"></span>
            </div>

            <%--LIST NOTES--%>
            <ul dnd-list="list.noteList" dnd-drop="noteDroped(list.listId, item, index)"
                dnd-external-sources="true">

                <li ng-repeat="note in list.noteList" class = "btn-default note" ng-click="onNoteClick(note)"
                    dnd-draggable="note"
                    dnd-moved="list.noteList.splice($index, 1)"
                    dnd-effect-allowed="move"
                    dnd-dragstart="dragStart(list.listId)">

                    <div class="title-in-list">{{ note.title }}</div>
                    <div class="description-in-list">{{ note.description }}</div>
                </li>
            </ul>

            <%--NEW NOTE--%>
            <div ng-hide="addNewNoteVisibility" ng-click= "addNewNoteVisibility = true" class = "btn-default note new-note-main-button">
                Add new note
            </div>
            <div class = "note new-note" ng-show="addNewNoteVisibility">
                <textarea rows="3" class="edit-input note-input" id="new-note-input" type="text" ng-model="newNoteText" placeholder="note"></textarea>
                <div ng-click="cancelAddingNewNote()" class="glyphicon glyphicon-remove glyph-red new-note-button"></div>
                <div ng-click="addNewNote(list.listId, list.noteList.length)" class="glyphicon glyphicon-ok glyph-green new-note-button"></div>
            </div>
        </div>
    </div>
</div>

<div class="row" ng-show="addNewListVisibility">
    <form class="form" ng-submit="addNewList()">
        <div class="inner-addon left-addon">
            <i class="glyphicon glyphicon-edit"></i>
            <input id="new-list-input" class="form-control" type="text" ng-model="newListName" placeholder="list name"/></br>
        </div>
        <button>ADD</button>
        <p class="message">
            <a href="" ng-click="hideNewListForm()">Cancel</a>
        </p>
    </form>
</div>

<div class="row" ng-hide="addNewListVisibility">
    <div class="col-md-2 col-centered" >
        <div ng-click="showNewListForm()" class="btn-default single-dashboard dsh-add">
            Add new list
        </div>
    </div>
</div>