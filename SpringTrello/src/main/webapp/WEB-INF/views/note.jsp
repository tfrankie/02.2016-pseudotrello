<div class="row note-row vertical-align">

    <div class="col-md-8">
        <div class="title-in-note">
            <div ng-hide="editButtonsVisibility">
                {{ note.title }}
            </div>
            <div ng-show="editButtonsVisibility">
                <textarea rows="2" class="edit-input note-input" id="edit-note-title" type="text" ng-model="editNoteTitle" placeholder="title"></textarea>
            </div>
        </div>

        <div class="description-in-note">
            <div ng-hide="editButtonsVisibility">
                {{ note.description }}
            </div>
            <div ng-show="editButtonsVisibility">
                <textarea rows="3" class="edit-input note-input" id="edit-note-description" type="text" ng-model="editNoteDescription" placeholder="description"></textarea>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div ng-click="editNoteClicked()" class = "btn-default note-button" ng-class="{'note-edit': !editButtonsVisibility, 'note-edit-clicked': editButtonsVisibility}">
            {{ editButtonText }}
        </div>
        <div ng-click="deleteNoteClicked()" class = "btn-default note-button note-delete">
            {{ deleteButtonText }}
        </div>
    </div>

</div>