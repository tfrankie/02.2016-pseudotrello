<div ng-include src="'sidebar'" ng-controller="sidebarCtrl"></div>
<div ng-include src="'navbar'" ng-controller="navbarCtrl"></div>

<div class="row dsh-row">
    <div ng-repeat="dashboard in dashboards" class="col-md-4">
        <div ng-click="onDashboardClick(dashboard.dashboardId)" class="btn-default dashboard dsh-title"
             ng-controller="dashboardButtonCtrl" ng-mouseleave="hideButtons()" ng-mouseover="showButtons()">

            <span ng-show="editButtonsVisibility || deleteButtonsVisibility" ng-click="cancelEditingDashboard($event)" class="glyphicon glyphicon-remove glyph-red inner-button"></span>
            <span ng-show="mainButtonsVisibility" ng-click="editDashboard($event)" class="glyphicon glyphicon-edit glyph-blue inner-button"></span>

            <input ng-show="editButtonsVisibility && !deleteButtonsVisibility" class="edit-input dsh-input" ng-click="cancelPagination($event)" id="edit-dashboard-input" type="text" ng-model="newDashboardName" placeholder="dashboard name"/>
            <span ng-hide="editButtonsVisibility || deleteButtonsVisibility">{{  dashboard.name  }} </span>
            <span ng-show="deleteButtonsVisibility" class="deleteDashboard" ng-click="cancelPagination($event)">DELETE THIS?</span>

            <span ng-show="editButtonsVisibility || deleteButtonsVisibility" ng-click="saveDashboard($event, dashboard.dashboardId)" class="glyphicon glyphicon-ok glyph-green inner-button"></span>
            <span ng-show="mainButtonsVisibility" ng-click="deleteDashboard($event)" class="glyphicon glyphicon-remove glyph-red inner-button"></span>
         </div>
    </div>
</div>

<div class="row" ng-show="addNewDashboardVisibility">
    <form class="form" ng-submit="addNewDashboard()">
        <div class="inner-addon left-addon">
            <i class="glyphicon glyphicon-edit"></i>
            <input id="new-name-input" class="form-control" type="text" ng-model="newDashboardName" placeholder="dashboard name"/></br>
        </div>
        <button>ADD</button>
        <p class="message">
            <a href="" ng-click="hideNewDashboardForm()">Cancel</a>
        </p>
    </form>
</div>

<div class="row" ng-hide="addNewDashboardVisibility">
    <div class="col-md-4 col-centered" >
        <div ng-click="showNewDashboardForm()" class="btn-default dashboard dsh-add">
            Add new dashboard
        </div>
    </div>
</div>