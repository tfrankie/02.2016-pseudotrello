<div>
    <nav class="navbar navbar-default" role="navigation">

        <a class="navbar-brand" href="#/dashboard">MyTrello</a>

        <div class="collapse navbar-collapse" id="navbar-collapse-1">

            <div class="nav navbar-nav navbar-left">
                <div class = "btn btn-default navbar-btn" ng-click="openSidebar()" style="position: relative">
                    {{ fullName }}
                </div>
            </div>

            <div class="nav navbar-nav navbar-right">
                <div class = "btn btn-default navbar-btn" ng-click="logout()" style="position: relative">
                    Logout
                </div>
            </div>

        </div>
    </nav>
</div>
