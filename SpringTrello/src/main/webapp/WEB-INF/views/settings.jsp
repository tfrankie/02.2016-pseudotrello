<form class="settings-form" ng-submit="changeName()">
    Change name
    <input id="edit-name-settings" class="form-control settings-input" type="text" ng-model="newNameSettings" placeholder="new name"/>
    <button>SUBMIT</button>
</form>

<hr>

<form class="settings-form" ng-submit="changePassword()">
    Change password
    <input id="old-password-settings" class="form-control" type="password" ng-model="oldPasswordSettings" placeholder="old password"/>
    <input id="new-password-settings" class="form-control" type="password" ng-model="newPasswordSettings" placeholder="new password"/>
    <input id="confirm-password-settings" class="form-control" type="password" ng-model="confirmPasswordSettings" placeholder="confirm new password"/>
    <button>SUBMIT</button>
</form>