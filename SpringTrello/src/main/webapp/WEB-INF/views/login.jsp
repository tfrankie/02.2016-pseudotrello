<div class="login-page">

	<flash-message duration="2000" show-close="true"></flash-message>

    <form class="form" ng-submit="login()" ng-hide="registerFormVisibility">
    
	    <div class="inner-addon left-addon">
	    	<i class="glyphicon glyphicon-user"></i>
			<input id="login-login" class="form-control" type="text" ng-model="enteredLogin" placeholder="username"><br>
		</div>
		<div class="inner-addon left-addon">
	    	<i class="glyphicon glyphicon-lock"></i>
			<input id="login-pass" class="form-control" type="password" ng-model="enteredPassword" placeholder="password"><br>
	    </div>
		<button>login</button>
      	<p class="message">Not registered? 
      		<a href="" ng-click="showRegisterForm()">Create an account</a>
      	</p>
    	
	</form>
  		
	<form class="form" ng-submit="register()" ng-show="registerFormVisibility" >

		<div class="inner-addon right-addon">
			<i ng-hide="validLoginText" class="glyphicon glyphicon-remove" style="color:red"></i>
			<i ng-show="validLoginText" class="glyphicon glyphicon-ok" style="color:green"></i>
			<input id="register-login" class="form-control" type="text" ng-model="enteredLogin" ng-change="onInputValueChanged()" placeholder="username"><br>
		</div>
		<div class="inner-addon right-addon">
			<i ng-hide="validPasswordText" class="glyphicon glyphicon-remove" style="color:red"></i>
			<i ng-show="validPasswordText" class="glyphicon glyphicon-ok" style="color:green"></i>
			<input id="register-pass" class="form-control" type="password" ng-model="enteredPassword" ng-change="onInputValueChanged()" placeholder="password"><br>
		</div>
		<div class="inner-addon right-addon">
			<i ng-hide="validRepeatedPasswordText" class="glyphicon glyphicon-remove" style="color:red"></i>
			<i ng-show="validRepeatedPasswordText" class="glyphicon glyphicon-ok" style="color:green"></i>
			<input id="register-rep-pass" class="form-control" type="password" ng-model="enteredRepeatedPassword" ng-change="onInputValueChanged()" placeholder="confirm password"><br>
		</div>

		<button>create</button>
		<p class="message">Already registered?
			<a href="" ng-click="showLoginForm()">Sign In</a>
		</p>

	</form>
</div>