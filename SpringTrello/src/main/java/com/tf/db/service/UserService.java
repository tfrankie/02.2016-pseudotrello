package com.tf.db.service;

import com.tf.db.dao.UserDao;
import com.tf.db.model.Dashboard;
import com.tf.db.model.User;
import com.tf.db.model.request.ChangeUserNameRequest;
import com.tf.db.model.request.ChangeUserPasswordRequest;
import com.tf.db.model.request.UserRequest;
import com.tf.db.model.response.ChangeUserNameResponse;
import com.tf.db.model.response.ChangeUserPasswordResponse;
import com.tf.db.model.response.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Created by frben on 16.04.2016.
 */
@Component
public class UserService {

    @Autowired
    private UserDao userDao;

    @Transactional
    public UserResponse registerUser(User registerRequest) {
        return userDao.registerUser(registerRequest);
    }

    @Transactional
    public UserResponse loginUser(UserRequest loginRequest) {
        return userDao.loginUser(loginRequest);
    }

    @Transactional
    public ChangeUserNameResponse changeUserName(ChangeUserNameRequest changeUserNameRequest) {
        return userDao.changeUserName(changeUserNameRequest);
    }

    @Transactional
    public ChangeUserPasswordResponse changeUserPassword(ChangeUserPasswordRequest changeUserPasswordRequest) {
        return userDao.changeUserPassword(changeUserPasswordRequest);
    }

}
