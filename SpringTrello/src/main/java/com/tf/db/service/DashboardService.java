package com.tf.db.service;

import com.tf.db.dao.DashboardDao;
import com.tf.db.model.Dashboard;
import com.tf.db.model.DashboardList;
import com.tf.db.model.request.*;
import com.tf.db.model.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by frben on 19.04.2016.
 */

@Component
public class DashboardService {

    @Autowired
    private DashboardDao dashboardDao;

    @Transactional
    public List<DashboardResponse> getDashboards(String userLogin) {
        List<Dashboard> userDashboards = dashboardDao.getDashboards(userLogin);
        List<DashboardResponse> dashboardsResponse = new ArrayList<DashboardResponse>();
        for(Dashboard dashboard: userDashboards){
            dashboardsResponse.add(new DashboardResponse(dashboard));
        }

        return dashboardsResponse;
    }

    @Transactional
    public List<DashboardListResponse> getLists(Integer dashboardId) {
        List<DashboardList> dashboardLists = dashboardDao.getLists(dashboardId);
        List<DashboardListResponse> dashboardListsResponse = new ArrayList<DashboardListResponse>();
        for(DashboardList dashboardList: dashboardLists){
            dashboardListsResponse.add(new DashboardListResponse(dashboardList));
        }
        return dashboardListsResponse;
    }

    @Transactional
    public DashboardResponse addNewDashboard(NewDashboardRequest dashboardRequest) {
        return dashboardDao.addNewDashboard(dashboardRequest);
    }

    @Transactional
    public DashboardListResponse addNewList(NewListRequest newList) {
        return dashboardDao.addNewList(newList);
    }

    @Transactional
    public DeleteDashboardResponse deleteDashboard(DeleteDashboardRequest deletingDashboard) {
        return dashboardDao.deleteDashboard(deletingDashboard);
    }

    @Transactional
    public EditDashboardResponse editDashboard(EditDashboardRequest editDashboardRequest) {
        return dashboardDao.editDashboard(editDashboardRequest);
    }

    @Transactional
    public DeleteListResponse deleteList(DeleteListRequest deletingList) {
        return dashboardDao.deleteList(deletingList);
    }

    @Transactional
    public EditListResponse editList(EditListRequest editListRequest) {
        return dashboardDao.editList(editListRequest);
    }

    @Transactional
    public NewNoteResponse addNewNote(NewNoteRequest newNoteRequest) {
        return dashboardDao.addNewNote(newNoteRequest);
    }

    @Transactional
    public void changeNotePosition(ChangeNotePositionRequest changeNotePositionRequest) {
        dashboardDao.changeNotePosition(changeNotePositionRequest);
    }

    @Transactional
    public DeleteNoteResponse deleteNote(DeleteNoteRequest deleteNoteRequest) {
        return dashboardDao.deleteNote(deleteNoteRequest);
    }

    @Transactional
    public EditNoteResponse editNote(EditNoteRequest editNoteRequest) {
        return dashboardDao.editNote(editNoteRequest);
    }
}
