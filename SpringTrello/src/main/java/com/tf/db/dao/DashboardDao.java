package com.tf.db.dao;

import com.tf.db.model.Dashboard;
import com.tf.db.model.DashboardList;
import com.tf.db.model.request.*;
import com.tf.db.model.response.*;

import java.util.List;

/**
 * Created by frben on 19.04.2016.
 */
public interface DashboardDao {

    List<Dashboard> getDashboards(String ownerLogin);

    List<DashboardList> getLists(Integer dashboardId);

    DashboardResponse addNewDashboard(NewDashboardRequest dashboardRequest);

    DashboardListResponse addNewList(NewListRequest newList);

    DeleteDashboardResponse deleteDashboard(DeleteDashboardRequest deletingDashboard);

    EditDashboardResponse editDashboard(EditDashboardRequest editDashboardRequest);

    DeleteListResponse deleteList(DeleteListRequest deletingList);

    EditListResponse editList(EditListRequest editListRequest);

    NewNoteResponse addNewNote(NewNoteRequest newNoteRequest);

    void changeNotePosition(ChangeNotePositionRequest changeNotePositionRequest);

    DeleteNoteResponse deleteNote(DeleteNoteRequest deleteNoteRequest);

    EditNoteResponse editNote(EditNoteRequest editNoteRequest);
}
