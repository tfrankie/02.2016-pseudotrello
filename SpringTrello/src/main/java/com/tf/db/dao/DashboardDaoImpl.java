package com.tf.db.dao;

import com.tf.db.model.Dashboard;
import com.tf.db.model.DashboardList;
import com.tf.db.model.Note;
import com.tf.db.model.User;
import com.tf.db.model.request.*;
import com.tf.db.model.response.*;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * Created by frben on 19.04.2016.
 */
@Repository
public class DashboardDaoImpl implements DashboardDao {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Dashboard> getDashboards(String ownerLogin) {
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.get(User.class, ownerLogin);

        Hibernate.initialize(user.getUserDashboards());

        return user.getUserDashboards();
    }

    public List<DashboardList> getLists(Integer dashboardId) {
        Session session = sessionFactory.getCurrentSession();
        Dashboard dashboard = (Dashboard) session.get(Dashboard.class, dashboardId);

        Hibernate.initialize(dashboard.getLists());

        for(DashboardList singleList: dashboard.getLists()){
            Collections.sort(singleList.getNoteList(), new Comparator<Note>() {
                public int compare(Note note1, Note note2) {
                    return note1.getPosition().compareTo(note2.getPosition());
                }
            });
        }

        return dashboard.getLists();
    }

    public DashboardResponse addNewDashboard(NewDashboardRequest dashboardRequest) {
        Session session = sessionFactory.getCurrentSession();

        User user = (User) session.get(User.class, dashboardRequest.getUser());

        Dashboard newDashboard = new Dashboard();
        newDashboard.setOwner(user);
        newDashboard.setName(dashboardRequest.getDashboardName());

        session.save(newDashboard);
        return new DashboardResponse(newDashboard);
    }

    public DashboardListResponse addNewList(NewListRequest newListRequest) {
        Session session = sessionFactory.getCurrentSession();

        Dashboard dashboard = (Dashboard) session.get(Dashboard.class, newListRequest.getDashboardId());

        DashboardList newList = new DashboardList();
        newList.setListName(newListRequest.getListName());
        newList.setDashboardParent(dashboard);

        session.save(newList);
        return new DashboardListResponse(newList);
    }

    public DeleteDashboardResponse deleteDashboard(DeleteDashboardRequest deletingDashboard) {
        Session session = sessionFactory.getCurrentSession();

        Dashboard dashboard = (Dashboard) session.get(Dashboard.class, deletingDashboard.getDashboardId());
        session.delete(dashboard);

        return new DeleteDashboardResponse(dashboard);
    }

    public EditDashboardResponse editDashboard(EditDashboardRequest editDashboardRequest) {
        Session session = sessionFactory.getCurrentSession();

        Dashboard dashboard = (Dashboard) session.get(Dashboard.class, editDashboardRequest.getDashboardId());
        dashboard.setName(editDashboardRequest.getName());
        session.save(dashboard);

        return new EditDashboardResponse(dashboard);
    }

    public DeleteListResponse deleteList(DeleteListRequest deletingList) {
        Session session = sessionFactory.getCurrentSession();

        DashboardList list = (DashboardList) session.get(DashboardList.class, deletingList.getListId());
        session.delete(list);

        return new DeleteListResponse(list);
    }

    public EditListResponse editList(EditListRequest editListRequest) {
        Session session = sessionFactory.getCurrentSession();

        DashboardList list = (DashboardList) session.get(DashboardList.class, editListRequest.getListId());
        list.setListName(editListRequest.getListName());
        session.save(list);

        return new EditListResponse(list);
    }

    public NewNoteResponse addNewNote(NewNoteRequest newNoteRequest) {
        Session session = sessionFactory.getCurrentSession();

        DashboardList parentList = (DashboardList) session.get(DashboardList.class, newNoteRequest.getListId());

        Note newNote = new Note();
        newNote.setListParent(parentList);
        newNote.setTitle(newNoteRequest.getTitle());
        newNote.setPosition(newNoteRequest.getPosition());

        session.save(newNote);
        return new NewNoteResponse(newNote);
    }

    public void changeNotePosition(ChangeNotePositionRequest changeNotePositionRequest) {
        Session session = sessionFactory.getCurrentSession();

        Note changingNote = (Note) session.get(Note.class, changeNotePositionRequest.getNoteId());

        DashboardList oldList = changingNote.getListParent();
        DashboardList newList = (DashboardList) session.get(DashboardList.class, changeNotePositionRequest.getListId());

        if(oldList != newList){
            for(Note singleNote: oldList.getNoteList()){
                if(singleNote.getPosition() > changingNote.getPosition()){
                    singleNote.setPosition(singleNote.getPosition() - 1);
                }
            }
            for(Note singleNote: newList.getNoteList()){
                if(singleNote.getPosition() >= changeNotePositionRequest.getPosition()){
                    singleNote.setPosition(singleNote.getPosition() + 1);
                }
            }
        } else {
            if(changingNote.getPosition() > changeNotePositionRequest.getPosition()){
                for(Note singleNote: oldList.getNoteList()){
                    if( (singleNote.getPosition() < changingNote.getPosition()) && (singleNote.getPosition() >= changeNotePositionRequest.getPosition()) ){
                        singleNote.setPosition(singleNote.getPosition() + 1);
                    }
                }
            } else {
                for(Note singleNote: oldList.getNoteList()){
                    if( (singleNote.getPosition() <= changeNotePositionRequest.getPosition()) && (singleNote.getPosition() > changingNote.getPosition()) ){
                        singleNote.setPosition(singleNote.getPosition() - 1);
                    }
                }
            }
        }

        changingNote.setListParent(newList);
        changingNote.setPosition(changeNotePositionRequest.getPosition());

        session.save(changingNote);
    }

    public DeleteNoteResponse deleteNote(DeleteNoteRequest deleteNoteRequest) {
        Session session = sessionFactory.getCurrentSession();

        Note note = (Note) session.get(Note.class, deleteNoteRequest.getNoteId());
        DashboardList parentList = note.getListParent();

        for(Note singleNote: parentList.getNoteList()){
            if(singleNote.getPosition() >= note.getPosition()){
                singleNote.setPosition(singleNote.getPosition() - 1);
            }
        }

        session.delete(note);

        return new DeleteNoteResponse(note);
    }

    public EditNoteResponse editNote(EditNoteRequest editNoteRequest) {
        Session session = sessionFactory.getCurrentSession();

        Note note = (Note) session.get(Note.class, editNoteRequest.getNoteId());

        note.setTitle(editNoteRequest.getTitle());
        note.setDescription(editNoteRequest.getDescription());

        session.save(note);

        return new EditNoteResponse(note);
    }
}
