package com.tf.db.dao;

import com.tf.db.model.User;
import com.tf.db.model.request.ChangeUserNameRequest;
import com.tf.db.model.request.ChangeUserPasswordRequest;
import com.tf.db.model.request.UserRequest;
import com.tf.db.model.response.ChangeUserNameResponse;
import com.tf.db.model.response.ChangeUserPasswordResponse;
import com.tf.db.model.response.UserResponse;


public interface UserDao {

    UserResponse registerUser(User registerRequest);

    UserResponse loginUser(UserRequest loginRequest);

    ChangeUserNameResponse changeUserName(ChangeUserNameRequest changeUserNameRequest);

    ChangeUserPasswordResponse changeUserPassword(ChangeUserPasswordRequest changeUserPasswordRequest);

}
