package com.tf.db.dao;

import com.tf.db.model.User;
import com.tf.db.model.request.ChangeUserNameRequest;
import com.tf.db.model.request.ChangeUserPasswordRequest;
import com.tf.db.model.request.UserRequest;
import com.tf.db.model.response.ChangeUserNameResponse;
import com.tf.db.model.response.ChangeUserPasswordResponse;
import com.tf.db.model.response.UserResponse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao{

    @Autowired
    private SessionFactory sessionFactory;
	@Autowired
	private PasswordEncoder passwordEncoder;

	public UserResponse registerUser(User registerRequest) {
		Session session = sessionFactory.getCurrentSession();

		User user = (User) session.get(User.class, registerRequest.getLogin());
		if(user==null){
			User newUser = new User();
			newUser.setLogin(registerRequest.getLogin());
			newUser.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
			session.persist(newUser);
			return new UserResponse(newUser);
		}
		return null;
	}

	public UserResponse loginUser(UserRequest loginRequest) {
		Session session = sessionFactory.getCurrentSession();
		User user = (User) session.get(User.class, loginRequest.getLogin());

		if(user!=null && passwordEncoder.matches(loginRequest.getPassword(), user.getPassword())){
			return new UserResponse(user);
		}
		return null;
	}

	public ChangeUserNameResponse changeUserName(ChangeUserNameRequest changeUserNameRequest) {
		Session session = sessionFactory.getCurrentSession();

		User user = (User) session.get(User.class, changeUserNameRequest.getLogin());
		user.setFullName(changeUserNameRequest.getFullName());
		session.save(user);

		return new ChangeUserNameResponse(user);
	}

	public ChangeUserPasswordResponse changeUserPassword(ChangeUserPasswordRequest changeUserPasswordRequest) {
		ChangeUserPasswordResponse response = new ChangeUserPasswordResponse();
		Session session = sessionFactory.getCurrentSession();

		User user = (User) session.get(User.class, changeUserPasswordRequest.getLogin());
		if(user.getPassword().equals(changeUserPasswordRequest.getOldPassword())){
			user.setPassword(changeUserPasswordRequest.getNewPassword());
			session.save(user);
		} else {
			response.setMessage("Password is incorrect");
		}

		return response;
	}
}
