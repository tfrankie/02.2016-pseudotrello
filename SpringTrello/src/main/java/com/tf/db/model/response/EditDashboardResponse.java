package com.tf.db.model.response;

import com.tf.db.model.Dashboard;

/**
 * Created by frben on 28.04.2016.
 */
public class EditDashboardResponse {

    int dashboardId;
    String name;

    public EditDashboardResponse(Dashboard dashboard) {
        dashboardId = dashboard.getDashboardId();
        name = dashboard.getName();
    }

    public int getDashboardId() {
        return dashboardId;
    }

    public void setDashboardId(int dashboardId) {
        this.dashboardId = dashboardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
