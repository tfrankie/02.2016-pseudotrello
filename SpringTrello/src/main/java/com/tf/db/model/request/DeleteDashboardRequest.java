package com.tf.db.model.request;

/**
 * Created by frben on 28.04.2016.
 */
public class DeleteDashboardRequest {

    int dashboardId;

    public int getDashboardId() {
        return dashboardId;
    }

    public void setDashboardId(int dashboardId) {
        this.dashboardId = dashboardId;
    }

}
