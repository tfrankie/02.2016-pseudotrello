package com.tf.db.model;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

/**
 * Created by frben on 19.04.2016.
 */

@Entity
@Table(name = "dashboards")
public class Dashboard {

    private int dashboardId;
    private User owner;
    private String name;
    private List<DashboardList> lists;

    @Id
    @GeneratedValue
    @Column(name="id")
    public int getDashboardId() {
        return dashboardId;
    }
    public void setDashboardId(int dashboardId) {
        this.dashboardId = dashboardId;
    }

    @JsonBackReference
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "login")
    public User getOwner() {
        return owner;
    }
    public void setOwner(User owner) {
        this.owner = owner;
    }

    @Column(name = "dashboard_name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy="dashboardParent", cascade = CascadeType.REMOVE)
    public List<DashboardList> getLists() {
        return lists;
    }
    public void setLists(List<DashboardList> lists) {
        this.lists = lists;
    }

    @Override
    public String toString() {
        return "Dashboard{" +
                "dashboardId=" + dashboardId +
                ", owner=" + owner +
                ", name='" + name + '\'' +
                '}';
    }
}
