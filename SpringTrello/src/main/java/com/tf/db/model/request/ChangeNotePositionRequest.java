package com.tf.db.model.request;

/**
 * Created by frben on 12.05.2016.
 */
public class ChangeNotePositionRequest {

    int noteId;
    int listId;
    int position;

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
