package com.tf.db.model.request;

/**
 * Created by frben on 29.04.2016.
 */
public class EditListRequest {

    int listId;
    String listName;

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

}
