package com.tf.db.model.request;

/**
 * Created by frben on 25.04.2016.
 */
public class NewListRequest {

    int dashboardId;
    String listName;

    public int getDashboardId() {
        return dashboardId;
    }
    public void setDashboardId(int dashboardId) {
        this.dashboardId = dashboardId;
    }

    public String getListName() {
        return listName;
    }
    public void setListName(String listName) {
        this.listName = listName;
    }

    @Override
    public String toString() {
        return "NewListRequest{" +
                "dashboardId=" + dashboardId +
                ", listName='" + listName + '\'' +
                '}';
    }
}
