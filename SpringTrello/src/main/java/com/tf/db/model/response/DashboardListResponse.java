package com.tf.db.model.response;

import com.tf.db.model.DashboardList;
import com.tf.db.model.Note;

import java.util.List;

/**
 * Created by frben on 24.04.2016.
 */
public class DashboardListResponse {

    int listId;
    String listName;
    List<Note> noteList;

    public DashboardListResponse(DashboardList dashboardList) {
        this.listId = dashboardList.getListId();
        this.listName = dashboardList.getListName();
        this.noteList = dashboardList.getNoteList();
    }

    public int getListId() {
        return listId;
    }
    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getListName() {
        return listName;
    }
    public void setListName(String listName) {
        this.listName = listName;
    }

    public List<Note> getNoteList() {
        return noteList;
    }
    public void setNoteList(List<Note> noteList) {
        this.noteList = noteList;
    }

    @Override
    public String toString() {
        return "DashboardListResponse{" +
                "listId=" + listId +
                ", listName='" + listName + '\'' +
                ", noteList=" + noteList +
                '}';
    }
}
