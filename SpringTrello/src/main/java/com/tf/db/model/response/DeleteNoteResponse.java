package com.tf.db.model.response;

import com.tf.db.model.Note;

/**
 * Created by frben on 16.05.2016.
 */
public class DeleteNoteResponse {

    int noteId;

    public DeleteNoteResponse(Note note) {
        noteId = note.getId();
    }

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

}
