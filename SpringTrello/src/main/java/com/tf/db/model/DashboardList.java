package com.tf.db.model;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

/**
 * Created by frben on 24.04.2016.
 */
@Entity
@Table(name = "lists")
public class DashboardList {

    int listId;
    Dashboard dashboardParent;
    String listName;
    List<Note> noteList;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public int getListId() {
        return listId;
    }
    public void setListId(int listId) {
        this.listId = listId;
    }

    @JsonBackReference
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "dashboard_id")
    public Dashboard getDashboardParent() {
        return dashboardParent;
    }
    public void setDashboardParent(Dashboard dashboardParent) {
        this.dashboardParent = dashboardParent;
    }

    @Column(name = "list_name")
    public String getListName() {
        return listName;
    }
    public void setListName(String listName) {
        this.listName = listName;
    }

    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, mappedBy="listParent", cascade = CascadeType.REMOVE)
    public List<Note> getNoteList() {
        return noteList;
    }
    public void setNoteList(List<Note> noteList) {
        this.noteList = noteList;
    }
}
