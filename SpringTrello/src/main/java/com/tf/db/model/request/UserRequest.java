package com.tf.db.model.request;

/**
 * Created by frben on 30.05.2016.
 */
public class UserRequest {

    String login;
    String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
