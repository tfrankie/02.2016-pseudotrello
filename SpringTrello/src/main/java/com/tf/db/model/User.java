package com.tf.db.model;

import org.codehaus.jackson.annotate.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    private String login;
    private String password;
    private String fullName;
    private List<Dashboard> userDashboards;

    @Id
    @Column
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }

    @Column
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    @Column
    public String getFullName() {
        return fullName;
    }
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy="owner", cascade = CascadeType.REMOVE)
    public List<Dashboard> getUserDashboards() {
        return userDashboards;
    }
    public void setUserDashboards(List<Dashboard> userDashboards) {
        this.userDashboards = userDashboards;
    }

    @Override
    public String toString() {
        return "User{" +
                "owner='" + login + '\'' +
                ", password='" + password + '\'' +
                ", fullName='" + fullName + '\'' +
                '}';
    }
}
