package com.tf.db.model.response;

import com.tf.db.model.User;

/**
 * Created by frben on 27.05.2016.
 */
public class ChangeUserNameResponse {

    String login;
    String fullName;

    public ChangeUserNameResponse(User user) {
        login = user.getLogin();
        fullName = user.getFullName();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

}
