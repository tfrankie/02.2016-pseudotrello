package com.tf.db.model.response;

import com.tf.db.model.Dashboard;

/**
 * Created by frben on 28.04.2016.
 */
public class DeleteDashboardResponse {

    int dashboardId;

    public DeleteDashboardResponse(Dashboard dashboard) {
        dashboardId = dashboard.getDashboardId();
    }

    public int getDashboardId() {
        return dashboardId;
    }

    public void setDashboardId(int dashboardId) {
        this.dashboardId = dashboardId;
    }
}
