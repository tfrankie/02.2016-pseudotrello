package com.tf.db.model.response;

import com.tf.db.model.User;

/**
 * Created by frben on 24.04.2016.
 */
public class UserResponse {

    private String login;
    private String fullName;

    public UserResponse() {
    }

    public UserResponse(User user){
        this.login = user.getLogin();
        this.fullName = user.getFullName();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "login='" + login + '\'' +
                ", fullName='" + fullName + '\'' +
                '}';
    }
}
