package com.tf.db.model;

import org.codehaus.jackson.annotate.JsonBackReference;

import javax.persistence.*;

/**
 * Created by frben on 24.04.2016.
 */

@Entity
@Table(name="notes")
public class Note {

    int noteId;
    DashboardList listParent;
    String title;
    String description;
    Integer position;

    @Id
    @GeneratedValue
    @Column
    public int getId() {
        return noteId;
    }
    public void setId(int id) {
        this.noteId = id;
    }

    @JsonBackReference
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "list_id")
    public DashboardList getListParent() {
        return listParent;
    }
    public void setListParent(DashboardList listParent) {
        this.listParent = listParent;
    }

    @Column(name = "note_title")
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "note_description")
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "position")
    public Integer getPosition() {
        return position;
    }
    public void setPosition(Integer position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Note{" +
                "noteId=" + noteId +
                ", listParent=" + listParent +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", position=" + position +
                '}';
    }
}
