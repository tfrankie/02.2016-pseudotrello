package com.tf.db.model.response;

import com.tf.db.model.Dashboard;

/**
 * Created by frben on 24.04.2016.
 */
public class DashboardResponse {

    private int dashboardId;
    private String name;

    public DashboardResponse(Dashboard dashboard) {
        this.dashboardId = dashboard.getDashboardId();
        this.name = dashboard.getName();
    }

    public int getDashboardId() {
        return dashboardId;
    }

    public void setDashboardId(int dashboardId) {
        this.dashboardId = dashboardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
