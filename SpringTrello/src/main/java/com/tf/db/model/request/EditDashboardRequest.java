package com.tf.db.model.request;

/**
 * Created by frben on 28.04.2016.
 */
public class EditDashboardRequest {

    int dashboardId;
    String name;

    public int getDashboardId() {
        return dashboardId;
    }

    public void setDashboardId(int dashboardId) {
        this.dashboardId = dashboardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
