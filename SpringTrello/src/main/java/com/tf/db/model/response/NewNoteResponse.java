package com.tf.db.model.response;

import com.tf.db.model.Note;

/**
 * Created by frben on 03.05.2016.
 */
public class NewNoteResponse {

    int noteId;
    int listId;
    int position;
    String title;

    public NewNoteResponse(Note newNote) {
        noteId = newNote.getId();
        listId = newNote.getListParent().getListId();
        title = newNote.getTitle();
        position = newNote.getPosition();
    }

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
