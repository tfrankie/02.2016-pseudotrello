package com.tf.db.model.request;

/**
 * Created by frben on 25.04.2016.
 */
public class NewDashboardRequest {

    String user;
    String dashboardName;

    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }

    public String getDashboardName() {
        return dashboardName;
    }
    public void setDashboardName(String dashboardName) {
        this.dashboardName = dashboardName;
    }

    @Override
    public String toString() {
        return "NewDashboardRequest{" +
                "user='" + user + '\'' +
                ", dashboardName='" + dashboardName + '\'' +
                '}';
    }
}
