package com.tf.db.model.request;

/**
 * Created by frben on 27.05.2016.
 */
public class ChangeUserNameRequest {

    String login;
    String fullName;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
