package com.tf.db.model.request;

/**
 * Created by frben on 17.05.2016.
 */
public class EditNoteRequest {

    int noteId;
    String title;
    String description;

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
