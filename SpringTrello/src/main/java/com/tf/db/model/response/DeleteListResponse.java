package com.tf.db.model.response;

import com.tf.db.model.DashboardList;

/**
 * Created by frben on 29.04.2016.
 */
public class DeleteListResponse {

    int listId;

    public DeleteListResponse(DashboardList list) {
        listId = list.getListId();
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }
}
