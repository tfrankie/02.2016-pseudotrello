package com.tf.db.model.request;

/**
 * Created by frben on 03.05.2016.
 */
public class NewNoteRequest {

    int listId;
    String title;
    Integer position;


    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "NewNoteRequest{" +
                "listId=" + listId +
                ", title='" + title + '\'' +
                ", position=" + position +
                '}';
    }
}
