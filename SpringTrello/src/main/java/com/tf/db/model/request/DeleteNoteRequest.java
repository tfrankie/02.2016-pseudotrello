package com.tf.db.model.request;

/**
 * Created by frben on 16.05.2016.
 */
public class DeleteNoteRequest {

    int noteId;

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }
}
