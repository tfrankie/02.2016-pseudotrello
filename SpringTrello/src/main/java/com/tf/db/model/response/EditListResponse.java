package com.tf.db.model.response;

import com.tf.db.model.DashboardList;

/**
 * Created by frben on 29.04.2016.
 */
public class EditListResponse {

    int listId;
    String listName;

    public EditListResponse(DashboardList list) {
        listId = list.getListId();
        listName = list.getListName();
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }
}
