package com.tf.db.model.response;

/**
 * Created by frben on 27.05.2016.
 */
public class ChangeUserPasswordResponse {

    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
