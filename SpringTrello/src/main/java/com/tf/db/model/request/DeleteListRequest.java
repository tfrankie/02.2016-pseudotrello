package com.tf.db.model.request;

/**
 * Created by frben on 29.04.2016.
 */
public class DeleteListRequest {

    int listId;

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }
}
