package com.tf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by frben on 16.04.2016.
 */
@Controller
public class RouteController {

//    @RequestMapping(value = "/")
//    public String home(){
//        return "index";
//    }

    @RequestMapping(value="/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/dashboard")
    public String dashboards() {
        return "dashboard";
    }

    @RequestMapping("/dashboard/{dashboardId}")
    public String singleDashboard(@PathVariable("dashboardId") int dashboardId) {
        System.out.println("RouteController Single Dashboard: " + dashboardId);
        return "singleDashboard";
    }

    @RequestMapping("/profile")
    public String profile() {
        return "profile";
    }

    @RequestMapping("/settings")
    public String settings() {
        return "settings";
    }

    @RequestMapping("/about")
    public String about() {
        return "about";
    }

    @RequestMapping("/navbar")
    public String navbar() {
        return "navbar";
    }

    @RequestMapping("/note")
    public String note() {
        return "note";
    }

    @RequestMapping("/sidebar")
    public String sidebar() {
        return "sidebar";
    }

}
