package com.tf.controller;

import com.tf.db.model.User;
import com.tf.db.model.request.*;
import com.tf.db.model.response.*;
import com.tf.db.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by frben on 19.04.2016.
 */
@RestController
public class RestDashboardController {

    @Autowired
    private DashboardService dashboardService;

    @RequestMapping(value = "/getDashboards", method = RequestMethod.POST)
    public ResponseEntity<List<DashboardResponse>> getDashboards(@RequestBody User user){

        List<DashboardResponse> userDashboards = dashboardService.getDashboards(user.getLogin());
        return new ResponseEntity<List<DashboardResponse>>(userDashboards, HttpStatus.OK);
    }

    @RequestMapping(value= "/getLists", method = RequestMethod.POST)
    public ResponseEntity<List<DashboardListResponse>> getLists(@RequestBody Integer dashboardId){

        List<DashboardListResponse> dashboardLists = dashboardService.getLists(dashboardId);
        return new ResponseEntity<List<DashboardListResponse>>(dashboardLists, HttpStatus.OK);
    }

    @RequestMapping(value = "/addNewDashboard", method = RequestMethod.PUT)
    public ResponseEntity<DashboardResponse> addNewDashboard(@RequestBody NewDashboardRequest newDashboard){

        DashboardResponse response = dashboardService.addNewDashboard(newDashboard);
        return new ResponseEntity<DashboardResponse>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/addNewList", method = RequestMethod.PUT)
    public ResponseEntity<DashboardListResponse> addNewDashboard(@RequestBody NewListRequest newList){

        DashboardListResponse response = dashboardService.addNewList(newList);
        return new ResponseEntity<DashboardListResponse>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/deleteDashboard", method = RequestMethod.POST)
    public ResponseEntity<DeleteDashboardResponse> deleteDashboard(@RequestBody DeleteDashboardRequest deletingDashboard){

        DeleteDashboardResponse response = dashboardService.deleteDashboard(deletingDashboard);
        return new ResponseEntity<DeleteDashboardResponse>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/editDashboard", method = RequestMethod.PUT)
    public ResponseEntity<EditDashboardResponse> editDashboard(@RequestBody EditDashboardRequest editDashboardRequest){

        EditDashboardResponse response = dashboardService.editDashboard(editDashboardRequest);
        return new ResponseEntity<EditDashboardResponse>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/deleteList", method = RequestMethod.POST)
    public ResponseEntity<DeleteListResponse> deleteList(@RequestBody DeleteListRequest deletingList){

        DeleteListResponse response = dashboardService.deleteList(deletingList);
        return new ResponseEntity<DeleteListResponse>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/editList", method = RequestMethod.PUT)
    public ResponseEntity<EditListResponse> editList(@RequestBody EditListRequest editListRequest){

        EditListResponse response = dashboardService.editList(editListRequest);
        return new ResponseEntity<EditListResponse>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/addNewNote", method = RequestMethod.PUT)
    public ResponseEntity<NewNoteResponse> addNewNote(@RequestBody NewNoteRequest newNoteRequest){

        NewNoteResponse response = dashboardService.addNewNote(newNoteRequest);
        return new ResponseEntity<NewNoteResponse>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/changeNotePosition", method = RequestMethod.PUT)
    public ResponseEntity<Void> changeNotePosition(@RequestBody ChangeNotePositionRequest changeNotePositionRequest){

        dashboardService.changeNotePosition(changeNotePositionRequest);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }


    @RequestMapping(value = "/deleteNote", method = RequestMethod.DELETE)
    public ResponseEntity<DeleteNoteResponse> deleteNote(@RequestBody DeleteNoteRequest deleteNoteRequest){

        DeleteNoteResponse response = dashboardService.deleteNote(deleteNoteRequest);
        return new ResponseEntity<DeleteNoteResponse>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/editNote", method = RequestMethod.PUT)
    public ResponseEntity<EditNoteResponse> editNote(@RequestBody EditNoteRequest editNoteRequest){

        EditNoteResponse response = dashboardService.editNote(editNoteRequest);
        return new ResponseEntity<EditNoteResponse>(response, HttpStatus.OK);
    }
}
