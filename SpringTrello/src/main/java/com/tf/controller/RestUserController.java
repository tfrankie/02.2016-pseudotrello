package com.tf.controller;

import com.tf.db.model.Dashboard;
import com.tf.db.model.User;
import com.tf.db.model.request.ChangeUserNameRequest;
import com.tf.db.model.request.ChangeUserPasswordRequest;
import com.tf.db.model.request.UserRequest;
import com.tf.db.model.response.ChangeUserNameResponse;
import com.tf.db.model.response.ChangeUserPasswordResponse;
import com.tf.db.model.response.UserResponse;
import com.tf.db.service.DashboardService;
import com.tf.db.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Set;


/**
 * Created by frben on 16.04.2016.
 */
@RestController
public class RestUserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<UserResponse> login(@RequestBody UserRequest loginRequest) {

        UserResponse response = userService.loginUser(loginRequest);
        if(response == null){
            return new ResponseEntity<UserResponse>(response, HttpStatus.UNAUTHORIZED);
        } else {
            return new ResponseEntity<UserResponse>(response, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<UserResponse> register(@RequestBody User registerRequest) {

        UserResponse response = userService.registerUser(registerRequest);
        if(response==null){
            return new ResponseEntity<UserResponse>(response, HttpStatus.CONFLICT);
        } else {
            return new ResponseEntity<UserResponse>(response, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/changeUserName", method = RequestMethod.PUT)
    public ResponseEntity<ChangeUserNameResponse> changeUserName(@RequestBody ChangeUserNameRequest changeUserNameRequest){

        ChangeUserNameResponse response = userService.changeUserName(changeUserNameRequest);
        return new ResponseEntity<ChangeUserNameResponse>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/changeUserPassword", method = RequestMethod.PUT)
    public ResponseEntity<ChangeUserPasswordResponse> addNewNote(@RequestBody ChangeUserPasswordRequest changeUserPasswordRequest){

        ChangeUserPasswordResponse response = userService.changeUserPassword(changeUserPasswordRequest);
        if(response.getMessage() == null){
            return new ResponseEntity<ChangeUserPasswordResponse>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<ChangeUserPasswordResponse>(response, HttpStatus.CONFLICT);
        }
    }

}
