CREATE TABLE notes
(
id int NOT NULL AUTO_INCREMENT,
list_id int,
note_title varchar(40),
note_description varchar(255),
primary key (id),
FOREIGN KEY (list_id) REFERENCES listslists(id)
)