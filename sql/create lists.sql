CREATE TABLE lists
(
id int NOT NULL AUTO_INCREMENT,
dashboard_id int,
list_name varchar(40),
primary key (id),
FOREIGN KEY (dashboard_id) REFERENCES dashboards(id)
)