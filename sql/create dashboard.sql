CREATE TABLE dashboards
(
id int NOT NULL AUTO_INCREMENT,
owner_login varchar(20),
dashboard_name varchar(40),
primary key (id),
FOREIGN KEY (owner_login) REFERENCES users(login)
)